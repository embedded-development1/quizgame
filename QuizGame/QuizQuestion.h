#include <QApplication>
#ifndef QUIZQUESTION_H
#define QUIZQUESTION_H

class QuizQuestion{
public:
    QuizQuestion();
    QuizQuestion(QString question, QString rightAnswer, QString wrongAnswer1, QString wrongAnswer2, QString wrongAnswer3);
    QString GetAnswer();
    QString GetQuestion();
    QString GetWrong1();
    QString GetWrong2();
    QString GetWrong3();
private:
    QString question;
    QString rightAnswer;
    QString wrongAnswer1;
    QString wrongAnswer2;
    QString wrongAnswer3;
};

#endif // QUIZQUESTION_H
