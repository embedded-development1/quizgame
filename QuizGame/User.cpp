#include "User.h"
#include <QApplication>

User::User(){
    name = "";
    score = 0;
}

void User::SetName(QString name){
    this->name = name;
}

QString User::GetName(){
    return name;
}

int User::GetScore(){
    return score;
}

int User::calculatePoints(int score, int time, int maxTime){
    double response = (double)time / (double)maxTime;
    response = response / 2;
    response = 1 - response;
    int result = score * response;
    return result;
}

void User::AddScore(int points){
    score += points;
}

void User::Reset(){
    name = "";
    score = 0;
}
