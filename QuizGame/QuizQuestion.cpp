#include "QuizQuestion.h"
#include <QApplication>

QuizQuestion::QuizQuestion(){
    question = "";
    rightAnswer = "";
    wrongAnswer1 = "";
    wrongAnswer2 = "";
    wrongAnswer3 = "";
}
QuizQuestion::QuizQuestion(QString question, QString rightAnswer, QString wrongAnswer1, QString wrongAnswer2, QString wrongAnswer3){
    this->question = question;
    this->rightAnswer = rightAnswer;
    this->wrongAnswer1 = wrongAnswer1;
    this->wrongAnswer2 = wrongAnswer2;
    this->wrongAnswer3 = wrongAnswer3;
}

QString QuizQuestion::GetQuestion(){
    return question;
}

QString QuizQuestion::GetAnswer(){
    return rightAnswer;
}

QString QuizQuestion::GetWrong1(){
    return wrongAnswer1;
}

QString QuizQuestion::GetWrong2(){
    return wrongAnswer2;
}

QString QuizQuestion::GetWrong3(){
    return wrongAnswer3;
}
