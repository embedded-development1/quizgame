#include "quizwindow.h"
#include "ui_quizwindow.h"
#include "User.h"
#include "QuizQuestion.h"
#include <QTimer>
#include <QtWidgets>

QuizWindow::QuizWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::QuizWindow)
{
    ui->setupUi(this);
    User user = User();
    QuizQuestion currentQuestion;
    prepareCountDown = 5;
    questionTimerLimit = 20;
    currentQuestionTime = 0;
    currentQuestionNum = 1;
    prepareTimer = new QTimer(this);
    questionTimer = new QTimer(this);
    connect(prepareTimer, &QTimer::timeout, this, &QuizWindow::UpdatePrepareLabel);
    connect(questionTimer, &QTimer::timeout, this, &QuizWindow::UpdateQuestionTimer);
    ui->stackedWidget->setCurrentWidget(ui->StartPage);
}

void QuizWindow::ReadFile(const QString &filename){
    QFileInfo info(filename);
    if(!info.exists() || !info.isFile()){
        MakeFile(filename);
    }
    QFile file(filename);
    if(!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("Application"),
                tr("Cannot read file %1:\n%2.")
                .arg(QDir::toNativeSeparators(filename),
                     file.errorString()));
        return;
    }

    QTextStream in(&file);
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    while(!in.atEnd()){
       QString newline = in.readLine();
       QStringList list = newline.split(QLatin1Char(','), Qt::SkipEmptyParts);
       QuizQuestion ques = QuizQuestion(list[0], list[1], list[2], list[3], list[4]);
       allQuestions.emplace_back(ques);
    }

    QGuiApplication::restoreOverrideCursor();
}

void QuizWindow::MakeFile(const QString &filename){

    QString errorMessage;
    QVector<QuizQuestion> listToBeWritten = MakeQuestions();
    QGuiApplication::setOverrideCursor(Qt::WaitCursor);
    QSaveFile file(filename);
    if(file.open(QFile::WriteOnly | QFile::Text)){
        QTextStream out(&file);
        for (int var = 0; var < listToBeWritten.size(); ++var) {
            out << listToBeWritten[var].GetQuestion() << "," << listToBeWritten[var].GetAnswer()
                << "," << listToBeWritten[var].GetWrong1() << "," << listToBeWritten[var].GetWrong2() << ","
                << listToBeWritten[var].GetWrong3() << '\n';
        }
        //Add code here//
        if(!file.commit()){
            errorMessage = tr("Cannot write file %1:\n%2.")
                    .arg(QDir::toNativeSeparators(filename), file.errorString());
        }
    } else {
        errorMessage = tr("Cannot open file %1 for writing:\n%2.")
                .arg(QDir::toNativeSeparators(filename), file.errorString());
    }
    QGuiApplication::restoreOverrideCursor();

    if(!errorMessage.isEmpty()){
        QMessageBox::warning(this, tr("Application"), errorMessage);
    }
}

QVector<QuizQuestion> QuizWindow::MakeQuestions(){
    QVector<QuizQuestion> list;
    QuizQuestion newQuestion;
    newQuestion = QuizQuestion("What category does C++ belongs to?","High-level programming language","Low-level programming language","Compiler","Operating System");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What is the compiler for C++ on linux?","g++","cpp","cmake","vc++");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What does C++ not support?","None of the answers","Multilevel inheritance","Hybrid inheritance","Hierarchical inheritance");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("Who created the C++ language?","Bjarne Stroustrup","Linus Torvalds","Dennis Ritchie","Anders Hejlsberg");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What other name does C++ have?","C with classes","Embedded C","High-level C","C with Object-Orientation");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What is C++ release-schedule?","3 years","2 years","4 years","5 years");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What does classes give?","All of the above","Inheritance","Encapsulation","Polymorphism");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What does C++ standard contain other than the core language?","Standard library","Standard algorithms","Exception handling","Standard classes");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("A subsript is a: ?","Number of the position in an array","The highest value in an array","Element in an array","Another name for array");
    list.emplace_back(newQuestion);
    newQuestion = QuizQuestion("What is the size of an int?","Depends on compiler","2 bytes","4 bytes","8 bytes");
    list.emplace_back(newQuestion);
    return list;
}

void QuizWindow::UpdateQuestion(){
    if(allQuestions.empty()){
        ReadFile("question.txt");
    }
    int select = QRandomGenerator::global()->bounded(0, allQuestions.size());
    currentQuestion = QuizQuestion(allQuestions[select].GetQuestion(), allQuestions[select].GetAnswer(),
                                   allQuestions[select].GetWrong1(),allQuestions[select].GetWrong2(),
                                   allQuestions[select].GetWrong3());
    allQuestions.removeAt(select);
    ui->Questionlabel->setText(currentQuestion.GetQuestion());

    QVector<int> randchoice;
    QVector<QPushButton *> buttonList;
    for (int var = 1; var < 5; ++var) {
        randchoice.emplace_back(var);
    }
    buttonList.emplace_back(ui->AnswerButton1);
    buttonList.emplace_back(ui->AnswerButton2);
    buttonList.emplace_back(ui->AnswerButton3);
    buttonList.emplace_back(ui->AnswerButton4);
    for (int var = 0; var < buttonList.size(); ++var) {
        int buttonSelect = QRandomGenerator::global()->bounded(0,randchoice.size());
        if(randchoice[buttonSelect] == 1){
            buttonList[var]->setText(currentQuestion.GetAnswer());
            randchoice.removeAt(buttonSelect);
        }
        else if(randchoice[buttonSelect] == 2){
            buttonList[var]->setText(currentQuestion.GetWrong1());
            randchoice.removeAt(buttonSelect);
        }
        else if(randchoice[buttonSelect] == 3){
            buttonList[var]->setText(currentQuestion.GetWrong2());
            randchoice.removeAt(buttonSelect);
        }
        else if(randchoice[buttonSelect] == 4){
            buttonList[var]->setText(currentQuestion.GetWrong3());
            randchoice.removeAt(buttonSelect);
        }
    }
}

void QuizWindow::StartPrepareTimer(){
    prepareTimer->start(1000);
}

void QuizWindow::UpdatePrepareLabel(){
    prepareCountDown--;
    if(prepareCountDown < 0){
        prepareTimer->stop();
        ui->stackedWidget->setCurrentWidget(ui->QuestionPage);
        ui->TimerLabel_2->setText(QString::number(questionTimerLimit));
        questionTimer->start(1000);
    }
    ui->TimerLabel->setText(QString::number(prepareCountDown));
}

void QuizWindow::UpdateQuestionTimer(){
    currentQuestionTime++;
    if(currentQuestionTime > questionTimerLimit){
        questionTimer->stop();
        QuizManager();
    }
    int timervalue = questionTimerLimit - currentQuestionTime;
    ui->TimerLabel_2->setText(QString::number(timervalue));
    int value = ui->progressBar->maximum() / questionTimerLimit;
    ui->progressBar->setValue(value * currentQuestionTime);
}

void QuizWindow::QuizManager(){
    if(currentQuestionNum < numOfQuestions){
        currentQuestionNum++;
        prepareCountDown = 5;
        ui->TimerLabel->setText(QString::number(prepareCountDown));
        ui->stackedWidget->setCurrentWidget(ui->PreparePage);
        UpdateQuestion();
        ui->AnswerButton1->setEnabled(true);
        ui->AnswerButton2->setEnabled(true);
        ui->AnswerButton3->setEnabled(true);
        ui->AnswerButton4->setEnabled(true);
        ui->AnswerButton1->setStyleSheet("");
        ui->AnswerButton2->setStyleSheet("");
        ui->AnswerButton3->setStyleSheet("");
        ui->AnswerButton4->setStyleSheet("");
        ui->AnswerLabel->setText("");
        currentQuestionTime = 0;
        ui->TimerLabel_2->setText(QString::number(questionTimerLimit));
        ui->progressBar->setValue(0);
    }
    else{
        ui->ScoreLabel_2->setText("Score: " + QString::number(user.GetScore()));
        ui->stackedWidget->setCurrentWidget(ui->ResultPage);
    }
}

void QuizWindow::GoToPreparePage(){
    ui->stackedWidget->setCurrentWidget(ui->PreparePage);
    animation = new QPropertyAnimation(ui->startButton, "geometry");
    QRect oldvalue = ui->startButton->geometry();
    QRect newValue = oldvalue;
    newValue.setY(-1000);
    animation->setDuration(0);
    animation->setStartValue(oldvalue);
    animation->setEndValue(newValue);
    animation->start();
}

QuizWindow::~QuizWindow(){
    delete ui;
}

void QuizWindow::on_startButton_clicked(){
    user.SetName(ui->UserTextBox->text());
    ui->PrepareLabel->setText("Get Ready " + user.GetName());
    ui->UsernameLabel->setText("Username: " + user.GetName());
    ReadFile("question.txt");
    UpdateQuestion();
    ui->TimerLabel->setText(QString::number(prepareCountDown));
    animation = new QPropertyAnimation(ui->startButton, "geometry");
    QRect oldvalue = ui->startButton->geometry();
    QRect newValue = oldvalue;
    newValue.setY(1000);
    animation->setDuration(2000);
    animation->setStartValue(oldvalue);
    animation->setEndValue(newValue);
    animation->start();
    QTimer::singleShot(3000, this, &QuizWindow::GoToPreparePage);
    //ui->stackedWidget->setCurrentWidget(ui->PreparePage);
}

void QuizWindow::on_UserTextBox_textChanged(const QString &arg1){
    if(arg1.isEmpty()){
        ui->startButton->setEnabled(false);
    }
    else if(ui->radioButton_3->isChecked() || ui->radioButton_5->isChecked() || ui->radioButton_10->isChecked()){
        ui->startButton->setEnabled(true);
    }
}

void QuizWindow::on_radioButton_3_clicked(){
    ui->radioButton_5->setChecked(false);
    ui->radioButton_10->setChecked(false);
    numOfQuestions = 3;
    if(!ui->UserTextBox->text().isEmpty()){
        ui->startButton->setEnabled(true);
    }
}

void QuizWindow::on_radioButton_5_clicked(){
    ui->radioButton_3->setChecked(false);
    ui->radioButton_10->setChecked(false);
    numOfQuestions = 5;
    if(!ui->UserTextBox->text().isEmpty()){
        ui->startButton->setEnabled(true);
    }
}

void QuizWindow::on_radioButton_10_clicked(){
    ui->radioButton_3->setChecked(false);
    ui->radioButton_5->setChecked(false);
    numOfQuestions = 10;
    if(!ui->UserTextBox->text().isEmpty()){
        ui->startButton->setEnabled(true);
    }
}

void QuizWindow::on_RetryButton_clicked(){
    currentQuestionNum = 1;
    ui->AnswerButton1->setEnabled(true);
    ui->AnswerButton2->setEnabled(true);
    ui->AnswerButton3->setEnabled(true);
    ui->AnswerButton4->setEnabled(true);
    ui->AnswerButton1->setStyleSheet("");
    ui->AnswerButton2->setStyleSheet("");
    ui->AnswerButton3->setStyleSheet("");
    ui->AnswerButton4->setStyleSheet("");
    ui->AnswerLabel->setText("");
    currentQuestionTime = 0;
    ui->TimerLabel_2->setText(QString::number(questionTimerLimit));
    ui->progressBar->setValue(0);

    ui->UserTextBox->setText("");
    ui->radioButton_3->setChecked(false);
    ui->radioButton_5->setChecked(false);
    ui->radioButton_10->setChecked(false);
    allQuestions.clear();

    prepareCountDown = 5;
    ui->TimerLabel->setText(QString::number(prepareCountDown));
    user = User();
    ui->stackedWidget->setCurrentWidget(ui->StartPage);
}

void QuizWindow::on_QuitButton_clicked(){
    QCoreApplication::quit();
}

void QuizWindow::on_stackedWidget_currentChanged(int arg1){
    if(arg1 == 1){
        StartPrepareTimer();
    }
}

void QuizWindow::on_AnswerButton1_clicked(){
    questionTimer->stop();
    if(ui->AnswerButton1->text() == currentQuestion.GetAnswer()){
        int score = user.calculatePoints(1000, currentQuestionTime, questionTimerLimit);
        user.AddScore(score);
        ui->AnswerLabel->setText("Right Answer! " + QString::number(score) + " points");
        ui->ScoreLabel->setText("Score: " + QString::number(user.GetScore()));
        ui->AnswerButton1->setStyleSheet("background-color:green");
    }
    else{
        ui->AnswerLabel->setText("Wrong Answer! 0 points");
        ui->AnswerButton1->setStyleSheet("background-color:red");
    }
    ui->AnswerButton1->setEnabled(false);
    ui->AnswerButton2->setEnabled(false);
    ui->AnswerButton3->setEnabled(false);
    ui->AnswerButton4->setEnabled(false);
    QTimer::singleShot(4000, this, &QuizWindow::QuizManager);
}

void QuizWindow::on_AnswerButton2_clicked(){
    questionTimer->stop();
    if(ui->AnswerButton2->text() == currentQuestion.GetAnswer()){
        int score = user.calculatePoints(1000, currentQuestionTime, questionTimerLimit);
        user.AddScore(score);
        ui->AnswerLabel->setText("Right Answer! " + QString::number(score) + " points");
        ui->ScoreLabel->setText("Score: " + QString::number(user.GetScore()));
        ui->AnswerButton2->setStyleSheet("background-color:green");
    }
    else{
        ui->AnswerLabel->setText("Wrong Answer! 0 points");
        ui->AnswerButton2->setStyleSheet("background-color:red");
    }
    ui->AnswerButton1->setEnabled(false);
    ui->AnswerButton2->setEnabled(false);
    ui->AnswerButton3->setEnabled(false);
    ui->AnswerButton4->setEnabled(false);
    QTimer::singleShot(4000, this, &QuizWindow::QuizManager);
}

void QuizWindow::on_AnswerButton3_clicked(){
    questionTimer->stop();
    if(ui->AnswerButton3->text() == currentQuestion.GetAnswer()){
        int score = user.calculatePoints(1000, currentQuestionTime, questionTimerLimit);
        user.AddScore(score);
        ui->AnswerLabel->setText("Right Answer! " + QString::number(score) + " points");
        ui->ScoreLabel->setText("Score: " + QString::number(user.GetScore()));
        ui->AnswerButton3->setStyleSheet("background-color:green");
    }
    else{
        ui->AnswerLabel->setText("Wrong Answer! 0 points");
        ui->AnswerButton3->setStyleSheet("background-color:red");
    }
    ui->AnswerButton1->setEnabled(false);
    ui->AnswerButton2->setEnabled(false);
    ui->AnswerButton3->setEnabled(false);
    ui->AnswerButton4->setEnabled(false);
    QTimer::singleShot(4000, this, &QuizWindow::QuizManager);
}

void QuizWindow::on_AnswerButton4_clicked(){
    questionTimer->stop();
    if(ui->AnswerButton4->text() == currentQuestion.GetAnswer()){
        int score = user.calculatePoints(1000, currentQuestionTime, questionTimerLimit);
        user.AddScore(score);
        ui->AnswerLabel->setText("Right Answer! " + QString::number(score) + " points");
        ui->ScoreLabel->setText("Score: " + QString::number(user.GetScore()));
        ui->AnswerButton4->setStyleSheet("background-color:green");
    }
    else{
        ui->AnswerLabel->setText("Wrong Answer! 0 points");
        ui->AnswerButton4->setStyleSheet("background-color:red");
    }
    ui->AnswerButton1->setEnabled(false);
    ui->AnswerButton2->setEnabled(false);
    ui->AnswerButton3->setEnabled(false);
    ui->AnswerButton4->setEnabled(false);
    QTimer::singleShot(4000, this, &QuizWindow::QuizManager);
}
