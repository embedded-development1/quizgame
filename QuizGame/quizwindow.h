#ifndef QUIZWINDOW_H
#define QUIZWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtWidgets>
#include "QuizQuestion.h"
#include "User.h"

QT_BEGIN_NAMESPACE
namespace Ui { class QuizWindow; }
QT_END_NAMESPACE

class QuizWindow : public QMainWindow
{
    Q_OBJECT

public:
    QuizWindow(QWidget *parent = nullptr);
    void ReadFile(const QString &filename);
    void MakeFile(const QString &filename);
    QVector<QuizQuestion> MakeQuestions();
    void UpdateQuestion();
    void StartPrepareTimer();
    void UpdatePrepareLabel();
    void UpdateQuestionTimer();
    void QuizManager();
    void GoToPreparePage();
    ~QuizWindow();

private slots:
    void on_startButton_clicked();

    void on_UserTextBox_textChanged(const QString &arg1);

    void on_radioButton_3_clicked();

    void on_radioButton_5_clicked();

    void on_radioButton_10_clicked();

    void on_RetryButton_clicked();

    void on_QuitButton_clicked();

    void on_stackedWidget_currentChanged(int arg1);

    void on_AnswerButton1_clicked();

    void on_AnswerButton2_clicked();

    void on_AnswerButton3_clicked();

    void on_AnswerButton4_clicked();

private:
    Ui::QuizWindow *ui;
    User user;
    QuizQuestion currentQuestion;
    QVector<QuizQuestion> allQuestions;
    QTimer *prepareTimer;
    QTimer *questionTimer;
    QPropertyAnimation *animation = nullptr;
    int prepareCountDown;
    int questionTimerLimit;
    int currentQuestionTime;
    int currentQuestionNum;
    int numOfQuestions;
};
#endif // QUIZWINDOW_H
