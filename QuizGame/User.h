#include <QApplication>
#ifndef USER_H
#define USER_H

class User{
public:
    User();
    int calculatePoints(int score, int time, int maxTime);
    void AddScore(int points);
    void SetName(QString name);
    QString GetName();
    int GetScore();
    void Reset();

private:
    QString name;
    int score;
};

#endif // USER_H
