# Quiz Game / C++ / Qt

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). 

The program is a quiz program made with Qt and C++, where you can answer trivia questions with four different options to choose from. The user can type their name and the amount of questions they want to answer. After a user has answered a question, points are given based on how quick the user respond and if it's the right question. The user can also add new questions in through a text file.
<br />

## Table of Contents
- [Background](#background)
- [Usage](#usage)
- [Install](#install)
- [Support](#support)
- [Contributors](#contributors)
- [Contributing](#contributing)
- [References](#references)
- [License](#license)

## Background
The layout and logic of the quiz program is based on [Kahoot](https://kahoot.com/)

Kahoot is a game-based learning platform, used as a educational tool in school and other institutions.
The games are user-generated multiple choice quizzes that can be created from different types of template and can then be hosted where the audience can connect via browser or phone. The questions are displayed on the hosts screen, while the audience answers on their connected device.
The audience answer the questions and gets points based on their performance.
Kahoot has been used to review students knowledge, formative assessment and as a break from traditional school activities.

### Assignment
Create GUI for test application

    Create mock up to GUI application inspired by the Kahoot;
    Application is used to ask quiz questions;
    Application is showing question and possible answers;
    Show current user name;
    Show the number of points gathered;
    Use your custom font in your elements;
    Commit to Repository. 
    * Consider adding animation to the GUI;

## Usage
The game starts with a screen with the title, description and two inputs where you write down your name and choose how many questions you want to answer.
</br> 
![](images/StartScreen.PNG)
</br>
After pressing start, a countdown will show up to prepare you for the question.
</br>
![](images/Prepare.PNG)
</br>
The question page will show up with the question, four possible answers and a timer at the bottom. Points are given based on if you answered correct and how quickly.
</br>
![](images/Question.PNG)
</br>
After finishing all the questions. The player are sent to result screen where they can see their score. They also have the choice to retry or quit the game.
</br>
![](images/Result.PNG)
</br>

## Install
- Download the files on this repository
- Install [Qt](https://www.qt.io/download-qt-installer?hsCtaTracking=99d9dd4f-5681-48d2-b096-470725510d34%7C074ddad0-fdef-4e53-8aa8-5e8a876d6ab4)
- Setup Qt (may or may not need a account)
- Open Qt and click on "Open project"
- Find the install file location and open "QuizGame.pro"
- Run the project

## Support
Send email to martin.dahren@se.experis.com for help with any problem you are having.

## Contributors
This is a project created by Martin Dahrén.

## Contributing
You are welcome to clone the project and make small changes to the program.
You can also fork the project and add new features if you give credit to the author. 
Bigger changes should have an issue opened to discuss what changes you want to do.

## License
[MIT](docs/LICENSE.md)